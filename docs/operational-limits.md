# **Limites Operacionais**

A harmony API possui alguns limites operacionais para que a sua utilização seja a melhor possível. Esses limites são baseados nos diferentes tipos de abordagem que o usuário pode utilizar para fazer as suas requisições. Aqui, separamos em três tópicos diferentes alguns dos tipos de abordagem que podem ser escolhidos pelos usuários da API. Essas abordagens são:

- [Abordagem 1](#abordagem-1)
    - Um local fazendo N requisições, uma após a outra com um intervalo de tempo entre elas.
- [Abordagem 2](#abordagem-2)
    - Um local fazendo N requisições simultâneas. Não é necessário esperar uma resposta para que a próxima requisição seja enviada.
- [Abordagem 3](#abordagem-3)
    - Um local abre blocos de N análises simultâneas. Onde cada bloco opera um número X de requisições, e então as respostas dessas requisições são retornadas para o usuário.

!!! info "Observação"
      Os limites operacionais da API foram medidos com ferramentas de testes de carga com o objetivo de simular alguns cenários de utilização. As tabelas deste documento apresentam parâmetros de referência para a sua aplicação. Os valores apresentados foram obtidos em condições específicas de utilização, ao integrar a sua aplicação com a API os valores podem ter pequenas divergências.

____

# **Abordagem 1**

## - Fluxo do Processo

![abordagem1](./images/abordagem1.png)

## - Limites

- Para N = 500
    - Endpoint: `/verifyImage`
    - Intervalo de tempo entre requisições: 0,5 segundos
  

|     Métrica     |           Valor           |
| :-------------: | :-----------------------: |
| Latência Média  |          1751 ms          |
| Latência Máxima |          2946 ms          |
| Latência Mínima |          1371 ms          |
|      Vazão      | 34 requisições por minuto |
|   Tempo Total   |         00:14:36          |



- Para N = 1000
    - Endpoint: `/verifyImage`
    - Intervalo de tempo entre requisições: 0,5 segundos
  

|     Métrica     |           Valor           |
| :-------------: | :-----------------------: |
| Latência Média  |          1783 ms          |
| Latência Máxima |          3782 ms          |
| Latência Mínima |          1361 ms          |
|      Vazão      | 33 requisições por minuto |
|   Tempo Total   |         00:29:45          |


- Para N = 1500
    - Endpoint: `/verifyImage`
    - Intervalo de tempo entre requisições: 0,5 segundos
  

|     Métrica     |           Valor           |
| :-------------: | :-----------------------: |
| Latência Média  |          1787 ms          |
| Latência Máxima |          6840 ms          |
| Latência Mínima |          1367 ms          |
|      Vazão      | 33 requisições por minuto |
|   Tempo Total   |         00:43:43          |

____

# **Abordagem 2**

## - Fluxo do Processo

![abordagem2](./images/abordagem2.png)

## - Limites

- Para N = 50
    - Endpoint: `/verifyImage`
    - Intervalo de tempo entre requisições: *Indiferente*
  

|     Métrica     |           Valor           |
| :-------------: | :-----------------------: |
| Latência Média  |         21236 ms          |
| Latência Máxima |         36729 ms          |
| Latência Mínima |          3020 ms          |
|      Vazão      | 1 requisição por segundo |
|   Tempo Total   |         00:00:36          |



- Para N = 100
    - Endpoint: `/verifyImage`
    - Intervalo de tempo entre requisições: *Indiferente*
  

|     Métrica     |          Valor           |
| :-------------: | :----------------------: |
| Latência Média  |         39752 ms         |
| Latência Máxima |         71560 ms         |
| Latência Mínima |         4844 ms          |
|      Vazão      | 1 requisição por segundo |
|   Tempo Total   |         00:01:13         |


!!! danger "Atenção"
        O limite para essa abordagem são **100 requisições simultâneas**. Valores maiores que esse podem ocasionar em erros referentes ao volume de chamadas.

____

# **Abordagem 3**

## - Fluxo do Processo

![abordagem3](./images/abordagem3.png)

## - Limites


- Para N = 10 e X = 50
    - Total de requisições: 500
    - Endpoint: `/verifyImage`
    - Intervalo de tempo entre requisições: 0,5 segundos
  

|     Métrica     |           Valor           |
| :-------------: | :-----------------------: |
| Latência Média  |          6757 ms          |
| Latência Máxima |         10410 ms          |
| Latência Mínima |          2186 ms          |
|      Vazão      | 2 requisições por segundo |
|   Tempo Total   |         00:05:39          |



- Para N = 10 e X = 100
    - Total de requisições: 1000
    - Endpoint: `/verifyImage`
    - Intervalo de tempo entre requisições: 0,5 segundos
  

|     Métrica     |          Valor           |
| :-------------: | :----------------------: |
| Latência Média  |         6772 ms          |
| Latência Máxima |         10661 ms         |
| Latência Mínima |         1490 ms          |
|      Vazão      | 2 requisições por segundo |
|   Tempo Total   |         00:11:21         |

- Para N = 100 e X = 5
    - Total de requisições: 500
    - Endpoint: `/verifyImage`
    - Intervalo de tempo entre requisições: 0,5 segundos
  

|     Métrica     |          Valor           |
| :-------------: | :----------------------: |
| Latência Média  |         61495 ms          |
| Latência Máxima |         75620 ms         |
| Latência Mínima |         2835 ms          |
|      Vazão      | 2 requisições por segundo |
|   Tempo Total   |         00:05:41         |


- Para N = 10 e X = 100
    - Total de requisições: 1000
    - Endpoint: `/verifyImage`
    - Intervalo de tempo entre requisições: 0,5 segundos
  

|     Métrica     |          Valor           |
| :-------------: | :----------------------: |
| Latência Média  |         6772 ms          |
| Latência Máxima |         10661 ms         |
| Latência Mínima |         1490 ms          |
|      Vazão      | 2 requisições por segundo |
|   Tempo Total   |         00:11:21         |

____

# **Formas de Abordagens não listadas**

Caso seja a abordagem escolhida não esteja entre as listadas e seja necessário mais informações sobre os limites operacionais sobre ela. Entre em contato com o email:
[suporte@biomtech.com.br](mailto:suporte@biomtech.com.br)
