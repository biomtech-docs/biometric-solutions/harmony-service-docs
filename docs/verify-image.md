# **Verificação da Imagem**

Esta API possui um padrão de retorno para cada situação encontrada para facilitar a manipulação dos resultados pelo integrador. Cada uma das respostas possuirá três pontos de observação, sendo eles:

* `Status code` da resposta
* Parâmetro `success` um boleano do json da resposta
* Parâmetro `message` uma descrição do resultado json da resposta

Além desses três pontos de observação ainda será retornado em seu json um parâmetro `successByDevice`. Esse parâmetro tem como objetivo apresentar se houve sucesso em algum dos dispositivos e em quais foram. Abaixo apresentamos como exemplo um json de sucesso da API:

```json
{
    "success": true,
    "message": "This picture can be send to device",
    "croppedImage":"<Imagem em base 64>",
    "successByDevice": {
        "A040": true,
        "A050": true,
        "A060": true
    }
}
```

!!! warning "Importante"
    O base64 retornado no parâmetro `croppedImage` ***deve ser*** a imagem enviada para o cadastro nos dispositivos, pois ela recebe um tratamento prévio que melhora a aceitação das mesmas. 


_____

# **Chamadas para API**

Para efetuar a requisição que analisa a qualidade da imagem que será enviada para o dispositivo, siga as instruções abaixo:

Caso seja necessário avaliar a imagem para todos os dispositivos que a API compreende, utilize o endpoint da seguinte forma:

| Método HTTP | Endpoint       |
| ----------- | -------------- |
| `POST`      | `/verifyImage` |


 No entanto, este endpoint também possibilita a análise da imagem para um dispositivo específico. Para isso, é necessário completar o caminho com Query Strings. As opções estão listadas abaixo:


| Método HTTP | Endpoint                        |
| ----------- | ------------------------------- |
| `POST`      | `/verifyImage?deviceModel=A040` |
| `POST`      | `/verifyImage?deviceModel=A050` |
| `POST`      | `/verifyImage?deviceModel=A060` |


!!! warning "Importante"
    * Ao utilizar as chamadas para um dispositivo específico. O parâmetro `successByDevice` não estará na resposta.
    * Caso seja enviado qualquer `deviceModel` diferente dos listados na tabela acima, será retornada a resposta padrão que é a análise para todos os dispositivos.


Será necessário enviar no corpo da requisição um `json` com os parâmetros da tabela abaixo:

| Parâmetro | Tipo   | Descrição                                           |
| --------- | ------ | --------------------------------------------------- |
| `faceId`  | String | Um Id para a face com no mínimo 5 caracteres        |
| `image`   | String | A imagem que deverá ser analisada em formato base64 |


Assim o json terá o seguinte aspecto:

```json
{
    "faceId": <Id da imagem>,
    "image": <Imagem em base64>
}
```

Você pode testar este endpoint no container utilizando o comando abaixo no seu terminal.

```json
curl --location --request POST 'https://harmony.biomtech.com.br/verifyImage' \
--header 'Content-Type: application/json' \
--data-raw '{
    "faceId": "Teste",
    "image": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAA8PDw8QDxETExEYGhcaGCQhHh4hJDYmKSYpJjZSMzwzMzwzUkhXR0JHV0iCZlpaZoKWfnd+lrWiorXk2eT///8BDw8PDxAPERMTERgaFxoYJCEeHiEkNiYpJikmNlIzPDMzPDNSSFdHQkdXSIJmWlpmgpZ+d36WtaKiteTZ5P/////AABEIAVwBYAMBIgACEQEDEQH/xACGAAABBQEBAAAAAAAAAAAAAAAAAQIDBAUGBxAAAgIBAwMDAwMDAwMFAAAAAAECAxEEITEFEkETIlEyYXEGUoFCYnIUIySRobEzNEOCwQEBAQEBAQAAAAAAAAAAAAAAAAECAwQRAQEAAgICAgIDAQEAAAAAAAABAhEDMSFBEjITIgRCUVJh/9oADAMBAAIRAxEAPwDswAQy0UAAAFEABQEFAAAAAAKGv11Wipc58+F8gS6vWU6StzslhHC9R6vdq87uNfiKKGt192qsc7JfhFKO+7AfnIyUk/wE5LBXct9nkqJHvzwNclx/2GuTUfyRZfhATrC4Q7MfsVvc+ZkkYR85YFhOHwhU34SGJJeGOz9gHb+Ug2YJv4Qjf3ATGOGGWhMy8TQ1ykuYgP7srdpjHFP7CJxYuWgBdy54+SxVbOuSlFuLXDRXTQ5N+Hn7EHd9J64rcU6h4l4kdOePxk08xeGdl0Xramo6e+W/EZBXXANTyhwAAAAAAAAABAAAFAAAAggAACiCgAAAAAAAogEdtka4SnJ4SWWwINZrKtHRK2x8cL5Z5vrNddrLXZa/wifqnUp629yziuO0EZSywFWW8kuCJMcsFENzeNiKMPjJPPcfTFPJKSK00Mw2WpxityBy+ADdfCH+7HMiJSefaOxP9zKJF+WKR9s/3seoS/cwiRP7A3EbhIPb8AG3yhP5Q7b4E2+ADHyhPxuANsKY8MTLXI/CkRtNfgB6mPT8rZlcdFkHfdC6urktNc/evpZ1SZ47CbjJNPDXk9E6L1RaypV2P/dgv+qA3hRBQAAAAEAAAAABQEABAAAAAAgUBAAUAAoDjv1H1LH/AA6395nQ9R1kdFpbLXzxH8nltls7bJzm8yk22yBucje5vZCNt/gEzQkiSqKxuyJNEqYCSiSafGWhM5Eg3CWTNWHWww+CnNP42NWaykyhODeSStWKy+ESKMxeNooSTaNMaPSflj+6HyV8N8jlNLYCbZi//cYmn/SG/wAAK+7w8kbbXMSRJDePJQzv/DDuX3Q2ePKGZ+G1+QiTnhhn9xHl+V/0FzlfKAc4jN0xctbrgXKZFOjJF/SamzT3Qtg8SgzMeUyaM90wPW9JqYaqiu6D2ki0cJ0HqHoXejJ+yw7tEAAAUAAAAAAQAAACAAgCgAAAogoAIxTL6trVo9JOefe1iJRxv6j1/wDqNW6Yv2VnO78ILJNycm8tvLGJsBzeEl5ETEDIEqx+WTR5K8WTxl8FE3AbMZHGflluCM1qHVuLi4jZVOTxgu11JrdF6umBz+TrMdsOOmcnhIWWjaN9Vdn9KY9QWPoJ81/G5SenxyiNwx4wdXZp1Je5GZbpkm8mpmzeNiZYndLJenTLwQumx7YNyudxQqS87iNw/Yy1CrD3SRJOiTWVJD5Q+NZrhF8ZRG4S8PJclXZHymQNy8xRZWbigy19hdiRtETTjwVC5aDZjVJA0QPz4Y5LtaxwyNSzsyRbfhhVqqbi18o9N6Zq1qtJVPO+MM8tj9jrf01qsW2UPia7kQduA1DgAAAoAAAAAAgQQAAUAAAAAADz39Tar1NTCCe0Ud5fPsqnLPg8k1lrtvtk23uBVyGRqYjlkocKo/LGoRyAkykSxyyGKbL1NEpYJbpqS1LVA1KaX8BRQo+NzUqpfLOOWbvhgbVS3yXYVpLgfGBIonPbtJIZ2iemibA4Krem/GCKVG74LyW4/CYZYktJ3eENWjiuWbTrXgTsRd1PjGE9JFP25yH+mXmJu9i+Brr+Bur8Ywp6OL4SKlvT/hHTOHyiCVTX3EyrNwlcXbo5pspyrlHbGUdzKmEzJ1OiT8HScjllxOVaTG5aNC7TuLKTTR2llcLLCcixeUNQ4B6bTTRsdOven1VNi47ln+TETLVcsYJR6/CSkk0PMvpV/r6KmX2wzUABRAAUQAAUQBQGgAAKAgAKIAAY/W7vS0V3+J5bJtne/qexrTQgn9cjgJMQNfwINHYNINyaupyCuBs6TTp4M5Zabxx2i02icnwbtWjUVuTVQUUkkWUjz5ZWvVjjISFUY8IsxQ2KJomGz0hcAh6KEwKSJCqI0mzMC4JO0Eim0bQEuAwQQ4EwTdomBoRYGNEzGNEVWlWuSCcM7NF0jaAwNVpeWkc9qKFF8bHb2QyjE1Wly21E3hlpyzx3HKSi0NT3LeoonXJ/BUeMHpl28tmqVktbfjkjW6Fg8SA779M6jupsqfhnWnnf6c1Hp6twb2kehohSgAAAAAAAAAgAAAAoAIAo1vZgcH+pZSlZV+Dj5bnR9dtVmpaXhHNvkQpUmhywvuyJyHRKL1O8lt/Bv6ZPBg0co6HSr2o5Zu3G0YInSI4E6OD0lRIhqHoinoliRIlRYiZDhiHo3GDhRBMlCiiCtgIRsexhFNYwexjMtGNDGPaGMgiaK1i2LTTIJphGBrqU45Oatj2tnW6yLUWzmL45bPRx15uSKkXhtCvdp+SP7fBJnKydXFqdNm46upr5PV4S7op/Y8e003C2uXlM9b00lKqDTymkzKrICgAgAAAAAAgAACiAAARWy7YSk+EmSmV1e/0tJNeWgPPOo2+pqZyMeby2WrZ985N+SpLksKTCJIEeMliH2LUjQ01e6bOjojhIydBT3e5m7HZHnzvl6uOeE8SaJBAsxRydj0OQmB6AciVDEPRRIhyGIkRqM0AKAQoCClCMjZIxhKsNYxj2MZlTGNY9jGiCNkMywyKSAyNXFuDZy16WXg7O+HdFo5TW19re252464ckY8ufuImLITj8Hd501b3TR6r0e31dFTL+3B5Knuek/pqzu0X4kZqunAQUAAQAFAAAQAAAAAAU5f8AUNvbpW/3vB07OL/U00oVr5kwRxZWl9TLdMHdfXWn9UkjY1vSK4V99XKFymNkrUwyyls9OcwT0rLRE0y1RHdGqzO3U6SKVUS4Q6ZYpiTHlvdevHqJYFuOyMqzVQp2W8ijZqr7PLLMbVucjplOHyh6a+TkvWuX9TQ5a66vyX4M/kdcmSJnL19XmvqRo19Vokvc8EuNizPGtpMdkoV6uqz6ZE/eZa7WMhkgUxe4bNJshkh7hO4bNJ8jWyBzKlmtrgwdNBsa2Y0+rQXCM+zqt0uMI1MbWbnjHTtojdkF5OTet1MnvJgp6iXhl+Cfk/8AHWZT4IpGBTqr6vDx8M06dZC/biS5Rm46WZJ5rJz/AFKvG50DMfqqXoZYx7iZfWuOsWHgTBLJJvdm/wBC6fTdY7rlmEeInptkm682ONyuo5g779KWN13Q/DOT6nXTXrr4VxxBT2R0f6T/APVuX9pLejWtu8FEXABAAAAoAACCCiAKAgoDX/8Ahwn6mnmVC/sO5m9n+Dz79RTzqoL4ggRkdLipa+r8M2bNdKLsqnFdrbSZidOl2a+n7to1dTDNzRzz+0enh+mTBsilN44yTUJOaDU1uMyfRQ7rYnTfhw1+zqKViuP4CbfEeSWKxEb2+TzvTENelgnmW7Lkaqv2Igc8EFmo7U22U8Lk6aZeCrPRVPwZ711kvpWF8shl1KEebJSfwtjUxyZuWCxZoV4K70so8SI11CqXMZr75Jq5uxZrsUvs+TWsozvClg7a2mje02oc4b8mLGb8pplqmeGYydMfDbjIkUilXNstxRydUmSNyHNMq2OSCI9Rc4xlgwpytfCNGyWSvKSj4yzpi55eVFae6b3LVehzyyG3Uqte+zH2RVXUq88WY+cnTWVc94R0FOnqq8LJcTh8I5iOvhJrF04/ndFqvV2/KkvsYuOTpjng3nGuSw4oqz0kFJTg8NEVWoUkWlLJnzFslSpbGZ1WOdN/JqRKXUYd2mkhO4zeq4hJqTR13T4uvS01r+pZZzMaZSsSxyzsaV2ekjfLfEjPDj5rjOof+71H+bN/9KzxqWvmBzesn3ai6XzZI2P05Z2a2s6+o4e69OXADY7xQ4IAAAFAAAQQAABRAAim85XyjzXrM+/XXHol0uxWv4ryeXa2XdfN/MiKqwn6d1U/2yTOqsgpaiLXD3OQlwjrtDP1tJTZ5iu1meSdV24b5yipqqFNjNBXi1lq+LGaNYk38sxL4sbyxm5W1Ef25RFF7FqC2MNRn2RaMnVNrng6d1pmfqNKuWaxqZRgU6aWpmnOeI/BDqtIqLZxUduUbsKFFZjySW1xtilZFPHk6zNyvG5eu7Vd0V2xwlj6Tc0HTvXrtmva/wClky0FG28ka9WIVxgpOKS4Rbkk476ZKh/8dqxPw/kV6eS3RqWVd8e31Xj4ZPXUlDDeTla7RRobeMmrXFMoxgoyNKngxZ5b9ElAo3I0p8FOUcsIyXTJ52KvZKcuyrd+ZHQyqUq3FPGSs6u2WVJJ/ZG5U1tzHUdA6JQk8vKM6u+2p9sdPDOXu1nk7PUJXQ7LFlGV/oqE898jpM/8cLxX2r9N0ULvV74JxwVtRpJ6O3NbzHPB0EO6FfZXBRiN/wBOrH7hclnGzdPYptNrtka9WHwTVadJLYtxgkjjbK6yVHBbFfUrNckXHsVr/pJCueq069VNvbJtzcaqp2eIQbKcI5bSHdUm6+mXfLwi/axvXwwtcNN53NHpUnHU1/5IzeS7on23RZ6cunhnb16l5giUgoeYRfzFMnIAQAAUBBQGgAAACBkDK6jb6cL3/YeZWt+pI9E63NR0t/y+2J51L6pfkk7a9IJG30XUYlbS+H7kYk+ZEmku9DUVWfD3/DLlN42GN1lK7G7DWSCpRUU1+8nhJSfY+HumU2pVXuL+mR58Xqy+rVRbgUYSzguwZKuKwtxXBSQkSZEVRnppLeJD6VnmBr4EaNIy1XP9pLGl+S9hsVQAghUPklFE3CIZgV8FykrMs0k9r6PkV3yWJrYhFCxQydKl4JYk3bksZ6ZcqGuGMdMvMUariNwgu2cqn+0mjWWsARUajgGPGMgikVL37GWpsrT90Vt5LEvarp4NPuwUuvzxo4R/dM2F3PCWyOT65qVbqVUuK0awm8oct1hWEuS1p9plVFzTr3JnovTxx6tom3pqG3zWi6Z3THnR0/PajQJEACCgAogAIwAQoAfDASbxF/ZEHJ/qCeIKPzNs4aT3Z1n6hszdCC8QORl5JGqjfkZ8jpeRng2xXTdM1Hr1Rg374Gs/ftNcHHdPt9LVVtvCbwztsRcMo8/JPjk9XFl8sdVHDZIvVyKFUsxa8plmDM10xaEGWIlKEi1BkaWUOwMiyXYsZpvagwOchjkUMnwVWyebKkpGapxZq5KsXktQCpJlbJYkVpbMVIkTLMWU1NE0ZokotYyMcQjIk5NIhaEaJGRSIsRsibHyZA2RpHYyFPYfNjVHuSQZnnJQ6jrlotO2vrltE4dylKTlJ5beWb36iwtRRBPiBgLx+T0cckxleblytz16h6XJco+pIrrj+S3p17/5NXpzj0rpr/2If4RNIzOnRxDHxFGkJ0lAogoAACgNEACgIrn/ALciQgt37U/n/wAEo4Prk09XZh8JI5pmz1Kfffc/uzFlwSNVGxEOfAw3GKabOj6tKqKham18mPyILjMpqrjlcbuOu0Wtr1F9kY/CZqpnG9Kn2a2H3TR12ThnjMa9PHlcvNXISLkJGVCZchM5u7QUh6kVFIcpkNLTkROxIrTtwVHOU2UWrL8vCY6MdtyljtaZaVqwESxWC1BbFGNsWy7GccIQp7RXmi0pQxuypOSyKQzs2ETaHKSGrfJFTwsLMZpmXJuDJoWiU1telMhchnfkY5FtNCTIZMJSIZSIt6R22KMZSfCRzdn6hxFqurf5Zsa2WNLe/wCxnAo7ceEs8x5OTPLG+KnsutvslZZLMmLHlEa5J4x9yOrik8L8suaZe6P3kirw1gtaVZnWv7iXpqPUNNFRqSRYIqViuC+yJQyAFAAFEABogCAGSjqZYf4jIumX1Cztrm/7ZIlI8+1r7pyfzIzpcF/VPKT+5QlwxGqY+CMlfBD5NxihcitBEcwHUTdV9U/iSO3jucLyjsdDardPVL7YZz5epXXhvmxO32snhMZOGUMgcXqi/GY92FZZBsy1s9tzZYhWsEUMRLEZBDJ1ZRlX1amH0TeDcyRzrUkVGFXfqYP3pM069UsZEnpslWWmknsXcPK+9Xkp26i2T9iEhp5vll+vTpIiqdL1MvqwasI4iLGCQ/gCOcE0Ue51ywzQkypbBMhs9WJg5lNNxeCXuyF2dKRC5ZCQRQS1n9Tl2aK37o4lcnWdcsS08YfMjk1yenj+rx8v2Sosx5iysi1DhGmIG/cjQ0W19OfFiM1mlo8Oypv5M5NR6hB5SHkdf0Q/xRIVkCiAAoCAA1iMBAEMDqtn/Gn+WbreEcr1Sb/0y+ZzZKsche90itPyi1evcipLdli03lET5Jo/SQvk1GaVcgwQSCFibfSLsd9T/KMWJLTY6bY2LwyZTcsaxurK7mHuRGvbNoi09qnFNPZonmvKPLXslWVHKGyj2sWiaZYlBSRlpmWaqup+6WCWvVQl/Wiaenqsi4zic7rumTg26pOJ0xmN9pfl6m3TRuh+5Eyur/cjk6HJxqTk1J7NMv8ApahOKTTyxcdJjlje2+7av3IfH0p8NGDKvUxnGLjyP7NRDfskTTpPhf7N3FcfKGO+qPkyY1aq1JqLwJLSahNLfLGj9P8ApqPV0/Ix6ur9xn2aC5OEXPdmP1GqyE/Sqbk/LLMdsXPD1uukeqq/ciL142SxF5Oc0fS7ZSi7Jt/Y6jT6WFMdkMpJ7T9vc0b6bbHdmC0opDJ7I5tKjQ7hClbVXRpqnOXCRYzXLdZu9TUqHiCMcfbN2znOXMnkb4iz1yakjxW7tqReCaL92CJL2odnEwqR8tGr0/Hq154MyfyjR0Dash/KM1Xpen2oqz+1E5Bp3mmt/MUTlZAAAAAAAxjRWIBDe+2qT+xynVYtPTw/lnUahtxwcv1GaesnLxXAzWo5W95tmVZck9m7kyCRqJSIhlyTfJE+SoX+kaKwRUSRX0itbiraMH9wnthkVodN1fZL0ZP8HTQn3I4OeYz2Ok0Gs9SKUuTlyY+47cWf9a2YvskaEJ5Rn7SRLVM416Iusr2pSjhomyRyWRK1LpnOqDa+xYh6kZxk90hk4hXZKB0ldPjx59xox1MJWQcoYwXHfQ1yjKhdB8xLSVMksmmL/H4/9yi9XqKIwSyuCKerrjKLUe4q9tSY12VpcEJ/H4992i2yycu9LBUcE5Nvdslna5v2okrr3yyWulmGE8YyJaau1Z8lgahTna490FW2RPZLCKeckA3hHK9Y1ffNURey3ka/UdatNV/e9oo4tycpOUnltnfix9vNy5+ieGHgHyxV9J3cFmCzFDJ8omr+iJDPkyqWPBqaJYuq+MmTFmppXmVX+WDNbj0vSvOnq/xROVNE86estlYAAACgIKBGxoMawIJt9xx2snmOrs+Z4Orum4ub+INnHav2aGL8zm2T2vpgS4ZESSI/JqBcbMgkW0sw/gq+QhrHwWWMJq171+CiSS9n4Yyfgme8JfkilwmQV5b4Lujzh45TKUvBc0D/AN2S+UMvquH2jptNd3xWefJdi8Mxo91cu5ceTSrmpxTTPLXqlaUXlCkNU8osIy2hlDJDKBe7Q9NNFXpm4aBZNB6dMjelkXbc5Morb/IKEpclpaWRNGjA3V/JkhhGK4RYSJFWkOwRzt2RCN4HMq22YCGWSyyhqtXXpq3Ob/C+SWUziNbqZ6i+bk9k2kjfHhuuXJn8Yj1Ops1NrsmyIYOXB6Xl7I+Ry4YzySxWzAsVN9hHP6h1T5Qj5iRS/Bo6XPdH/JGcaemfvjj5M5Nx6H02WdNH+TRMnpkv9v7dzNUsYpQAQBwIQUCEaxRr4bCMvWSxTqJfY5DqDxRp18xOp18u3Ry+7OU6k4/7MVyq1kk7bYs/AnD/AIHTGeUzURN/QVmWP6P4IWBEWYL3r/EgiWoLZMIFvVP8jMd0GEH/ALdosPpQpFea2JNI8XJjJc/yFDxYh6pPtHSw3Q+uTql/aM07zFFmVeUeavXFiFiUkzSjLJgLuh+C/p71w2SxZWxHA/BUhZgtqSZG0iH4GIcmUOwhGhciNgMGsVshlPBA22aijNcnNi22OyWFwOUVFFZVr5Kuqb+Is4Rs7Dqc+zS2/jBxx34urXm5u5AAgrOriUmgiFE9fkiw6O0xf6xsvq/gkxlKRGjfJoaf6kynjKyT1N9yfyZyaj0Hp8s1PC3UzZjJSWTD6VYp1pqOE4o2Ppl9mIzUwCAVCgAAQkc17cEzRk6/XQog0nuCM3quoXpRrXJyuqn3YLVl85vvl53wUJGY2pPyMXKJZ8si8o0ymT9n8ET2iPj9P/UbyihqRNB4hIZjdfgkjtFhEcNq5j19KI+ICyljAVHzuJT9YmXhklMdxeqTuOh0nBqqLaMvR8GzXweXLt6selaVZA4NPY1OxMilUTbWkNdzW0i/XcZ0oESnKHBSWx0EbUyXvRhQ1UXzlMsLVR/cg1tq+ohjsMt6uPyRvUt8IG2nK5FG25zfbEgzOfLLEIETsQhhD2SqJHMbHP8AWn/x/wAyRyp218arrFTPiSZx99MqLp1S5i8Hp4vq8nL9kICsDo5BE0HvIhRJHlkqxO+Yjo/Q0Rp8L5RJHl/cjR0Nyzp8N9r2IYrccm4STJVjuOizzS454OhW5yHSJPunJJtcs66tpxTTymZxMu0qFwCQ/BpkwBRAMrqWvr0sccz+DiNRqJXyc5vK+Pkgv1U7bZObbbfkgs1NS4eX4Jd1rWk7nGCzPeT8FOTzIg78vLYqkWQJPkhJGMwA6L5X3BcEal70TpfUVBLZJg37YoY98oVPMgEm8L+RMOSYWc4+Cxpo90sfYCOFeUi1CpLgktp9Ka+JLMSeuOTnlbHTGSrWmWDZr3RmUxwalRwrvFiKHOIsUPRltVnWU7KjWaIZQKmmN6eGP9Muzr+wigNmlRVk0aywoEqgNiKECxGOBySQpFNZWsZYZVs4LErB1s3HU0y+5B1qjenUfuXbL8om1se43LtGtXoXBcygmvyj08by8nbgGNJZQabj5RHg6OREOj9QgseQH/tJ1Lh/JWXBPHholWLSzlCzj2oZDhbliS7lhmW250G1uc4HXU91c5eYnBdGs9PWRPQX7cSXj/wIlXEKMg1hNcMcVkNiDJSwZmq6hTXNVyswybWR55qJRssc5RSb+Bk665wTgl7Y7ryVL2+4ISaWUyrv0hUd2OUg8sIrcqHETeWSz2WCEBH4ZZTWV8MqkkX7V+QHy9sxa+WxLOIsTiDAY3lsuaeXpzrn4T3KqLdaJSOtu0K1WjzD64bxMSnc2ehXWSrtrb2jwVOowjVrYuCx3rLM5zc23x3V0krRfrKVZcgeevTF2JIQwZMjKgMCgBHKCZC4NMsgBAkPBgFKIwGgNkVbCzIqWFiVk6qLeEvJ2FMI1V1p/ZHM9qlqdOnw7EdXcv8Ajs9HH1Xm5e44nr/T/Q1PrQXss/7SOaksNnqfUqoXdPu71xDKPNJpZOtcYqDVyP8AkjAeuCavwQ/0odliqtrbYs1yzyU2/amOTeEzDbQ0/s1UWej0zU4Qy+Uea0NucPszvtLJz0lM3yoiJk06niDXjLJkynppOSln7FlGmVbV2+lVOfwjzm3VSssnKTy5M7Trc5R0skjzyX1ozGuo/9k="
}'
```

_______

# **Respostas da API**


## **Retorno de sucesso**


| Ponto de observação | Valor                                | Descrição                                                   |
| ------------------- | ------------------------------------ | ----------------------------------------------------------- |
| `Status Code`       | `200`                                | Status code de sucesso na requisição                        |
| `success`           | `True`                               | Indicador booleano de sucesso na requisição                 |
| `message`           | `This picture can be send to device` | Mensagem descritiva do resultado da requisição              |
| `croppedImage`      | `<base 64 image>`                    | Imagem recortada na região da face em base64                |
| `successByDevice`   | `Json Object`                        | Json indicando o resultado da análise para cada dispositivo |



__________________________________

## **Retornos de falha**


Há vários retornos de falhas possíveis associados a imagem enviada, todos eles estarão listados neste tópico. Também serão listados os retornos de falhas relacionados a requisição.


### - *Retornos relacionados à falha na requisição*

| Ponto de observação |          Valor           |                 Descrição                 |
| :-----------------: | :----------------------: | :---------------------------------------: |
|    `Status Code`    |          `400`           |                Bad Request                |
|      `success`      |         `False`          | Indicador booleano de falha na requisição |
|      `message`      | `Invalid json was sent.` | O JSON enviado na requisição não é válido |


| Ponto de observação |           Valor            |                 Descrição                 |
| :-----------------: | :------------------------: | :---------------------------------------: |
|    `Status Code`    |           `405`            |            Method not Allowed             |
|      `success`      |          `False`           | Indicador booleano de falha na requisição |
|      `message`      | `HTTP Method must be post` |  Método HTTP da requisição não é válido   |


| Ponto de observação |                           Valor                           |                     Descrição                      |
| :-----------------: | :-------------------------------------------------------: | :------------------------------------------------: |
|    `Status Code`    |                           `409`                           |                      Conflict                      |
|      `success`      |                          `False`                          |     Indicador booleano de falha na requisição      |
|      `message`      | `We had some erros handling A040/A050 container response` | Problema em interpretar as respostas do harmony040 |


| Ponto de observação |                        Valor                         |                     Descrição                      |
| :-----------------: | :--------------------------------------------------: | :------------------------------------------------: |
|    `Status Code`    |                        `409`                         |                      Conflict                      |
|      `success`      |                       `False`                        |     Indicador booleano de falha na requisição      |
|      `message`      | `We had some erros handling A060 container response` | Problema em interpretar as respostas do harmony060 |


| Ponto de observação |                             Valor                              |                    Descrição                    |
| :-----------------: | :------------------------------------------------------------: | :---------------------------------------------: |
|    `Status Code`    |                             `503`                              |               Service Unavailable               |
|      `success`      |                            `False`                             |    Indicador booleano de falha na requisição    |
|      `message`      | `Couldn't do request to A060 container, check if it's running` | Não foi possível se comunicar com o harmonya060 |


| Ponto de observação |                                Valor                                |                    Descrição                    |
| :-----------------: | :-----------------------------------------------------------------: | :---------------------------------------------: |
|    `Status Code`    |                                `503`                                |               Service Unavailable               |
|      `success`      |                               `False`                               |    Indicador booleano de falha na requisição    |
|      `message`      | `Couldn't do request to A040/A050 container, check if it's running` | Não foi possível se comunicar com o harmonya040 |



| Ponto de observação | Valor |      Descrição       |
| :-----------------: | :---: | :------------------: |
|    `Status Code`    | `404` | Unprocessable Entity |




### - *Retornos relacionados à falhas na imagem enviada*

| Ponto de observação | Valor         | Descrição                                                                       |
| ------------------- | ------------- | ------------------------------------------------------------------------------- |
| `Status Code`       | `412`         | Precondition Failed                                                             |
| `success`           | `False`       | Indicador booleano de falha na requisição                                       |
| `message`           | `Bad picture` | A imagem não atende os requisitos de qualidade para enviar para os dispositivos |


__________________________________


## **Retornos não identificados**

Caso seja identificado algum retorno de falha, por favor entre em contato com o suporte técnico através do email:
[suporte@biomtech.com.br](mailto:suporte@biomtech.com.br)