# Bem-vindo a documentação da Harmony API

A **Harmony API** é uma solução desenvolvida e fornecida pela **Biomtech Facial Recognition Solutions**.

## Introdução

Esta API tem como principal objetivo avaliar a qualidade das imagens que serão enviadas para os dispositivos de reconhecimento facial, fornecidos ou homologados pela biomtech, antes que elas sejam submetidas para os mesmos. Além disso, as APIs irão retornar uma imagem com um recorte na região da face do usuário visando aumentar a aceitação das imagens pelos dispositivos e reduzir o tamanho da imagem trafegada.

Essa solução consiste em um servidor disponibilizado na nuvem onde os integradores podem realizar chamadas HTTP que retornarão as informações desejadas. A URL base que deverá ser chamado por quem fizer a integração deve ser:

```
https://harmony.biomtech.com.br/
```

A URL base contém informações sobre os caminhos da API. Com exceção da chamada à URL base todas as outras chamadas deverão ser feitas utilizando o método `POST`, e todos os dados serão retornados em `JSON`

Ao fazer uma chamada na URL base utilizando um método `GET`, a seguinte resposta deve ser obtida:
```json
{
    "info": "All requests must be done using method POST with a json body. The image parameter must be encoded using base64.",
    "verifyImage": {
        "info": "You have to send a json body containing two parameters: faceId and image. This image must be encoded with base64. If you don't have a faceId to your images,you can send any string with length > 5 as a faceId",
        "Path": "/verifyImage"
    },
    "faceCrop": {
        "info": "You have to send a json body containing two parameters: faceId and image. This image must be encoded with base64. If you don't have a faceId to your images,you can send any string with length > 5 as a faceId",
        "Path": "/faceCrop"
    },
    "modelImprovement": {
        "info": "You have to send a json body containing two parameters: deviceModel and image. This image must be encoded with base64. Only three devices models are accepted, if you send values different of A040,A050 or A060 the answer will be a Bad Request",
        "Path": "/modelImprovment"
    }
}
```

As possibilidades de utilização da solução estarão descritas nos seus respectivos tópicos listados abaixo.

* [Verificação da qualidade da imagem](verify-image.md)
* [Recorte da imagem na região da face](face-crop.md)
* [Submissão da imagem para melhoria do modelo](model-improvment.md)

Os limites operacionais da API estão descritos no tópico listado abaixo:

* [Limites Operacionais](operational-limits.md)